package br.edu.iftm.pdm.pdmfood.ui.lists.stores;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import br.edu.iftm.pdm.pdmfood.R;
import br.edu.iftm.pdm.pdmfood.model.Store;

public class StoreDataViewHolder extends RecyclerView.ViewHolder
                                    implements View.OnClickListener {

    private final TextView txtDetailStoreName;
    private final TextView txtDetailAddress;
    private final StoresDataAdapter adapter;
    private Store currentStore;

    public StoreDataViewHolder(@NonNull View itemView, StoresDataAdapter adapter) {
        super(itemView);
        this.txtDetailStoreName = itemView.findViewById(R.id.txtDetailStoreName);
        this.txtDetailAddress = itemView.findViewById(R.id.txtDetailAddress);
        this.adapter = adapter;
        itemView.setOnClickListener(this);
    }

    public void bind(Store store) {
        this.txtDetailStoreName.setText(store.getName());
        this.txtDetailAddress.setText(store.getAddress());
        this.currentStore = store;
    }

    @Override
    public void onClick(View v) {
        if(this.adapter.getOnClickStoreListener() != null) {
            this.adapter.getOnClickStoreListener().onClickStore(this.currentStore);
        }
    }
}
