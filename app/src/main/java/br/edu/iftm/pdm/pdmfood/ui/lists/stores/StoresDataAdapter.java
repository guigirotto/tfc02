package br.edu.iftm.pdm.pdmfood.ui.lists.stores;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import br.edu.iftm.pdm.pdmfood.R;
import br.edu.iftm.pdm.pdmfood.model.Store;

public class StoresDataAdapter extends RecyclerView.Adapter<StoreDataViewHolder> {

    private ArrayList<Store> stores;
    private OnClickStoreListener listener;

    public interface OnClickStoreListener {
        void onClickStore(Store store);
    }

    public StoresDataAdapter(ArrayList<Store> stores) {
        this.stores = stores;
        this.listener = null;
    }

    @NonNull
    @Override
    public StoreDataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.store_item_view, parent, false);
        return new StoreDataViewHolder(itemView, this); // passo 3 + passo 4
    }

    @Override
    public void onBindViewHolder(@NonNull StoreDataViewHolder holder, int position) {
        holder.bind(this.stores.get(position));
    }

    @Override
    public int getItemCount() {
        return this.stores.size();
    }

    //----------- METODOS AUXILIARES

    public void setOnClickStoreListener(OnClickStoreListener listener) {
        this.listener = listener;
    }

    public OnClickStoreListener getOnClickStoreListener() {
        return this.listener;
    }

}
