package br.edu.iftm.pdm.pdmfood.data;

import java.util.ArrayList;

import br.edu.iftm.pdm.pdmfood.model.Store;

public class DummyData {

    public static void createData(ArrayList<Store> stores) {
        Store s;
        s = new Store("Portuguesa", "Presunto, ovo, queijo, tomate");
        stores.add(s);
        s = new Store("Calabresa", "Calabresa, queijo, cebola");
        stores.add(s);
        s = new Store("Baiana", "Presunto, queijo, ovo, pimenta");
        stores.add(s);
        s = new Store("Lombo", "Lombo, catupiry, queijo");
        stores.add(s);
        s = new Store("Franco c/ catupiry", "Frango, catupiry, queijo");
        stores.add(s);
    }
}
