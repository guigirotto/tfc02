package br.edu.iftm.pdm.pdmfood.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Store implements Parcelable {

    private String name;
    private String owner;
    private String ssn;
    private String phoneNumber;
    private String email;
    private String address;

    public Store(String name, String descricao) {
        this.name = name;
        this.owner = descricao;
        this.ssn = ssn;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public String getOwner() {
        return owner;
    }

    public String getSsn() {
        return ssn;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public String getAddress() {
        return address;
    }

    //------------ IMPLEMENTACAO DO PARCELABLE

    protected Store(Parcel in) {
        name = in.readString();
        owner = in.readString();
        ssn = in.readString();
        phoneNumber = in.readString();
        email = in.readString();
        address = in.readString();
    }

    public static final Creator<Store> CREATOR = new Creator<Store>() {
        @Override
        public Store createFromParcel(Parcel in) {
            return new Store(in);
        }

        @Override
        public Store[] newArray(int size) {
            return new Store[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(owner);
        dest.writeString(ssn);
        dest.writeString(phoneNumber);
        dest.writeString(email);
        dest.writeString(address);
    }
}
