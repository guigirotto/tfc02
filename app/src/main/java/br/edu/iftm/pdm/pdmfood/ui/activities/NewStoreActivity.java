package br.edu.iftm.pdm.pdmfood.ui.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import br.edu.iftm.pdm.pdmfood.R;
import br.edu.iftm.pdm.pdmfood.model.Store;

public class NewStoreActivity extends AppCompatActivity {

    public static final String RESULT_KEY = "NewStoreActivity.RESULT_KEY";
    private TextView etxtStoreName;
    private TextView etxtOwner;
    private TextView etxtSSN;
    private TextView etxtPhone;
    private TextView etxtEmail;
    private TextView etxtAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_store);
        this.etxtStoreName = findViewById(R.id.nomepizza);
        this.etxtPhone = findViewById(R.id.preco);
        this.etxtAddress = findViewById(R.id.igpizza);
    }

    public void onClickSave(View view) {
        String storeName = this.etxtStoreName.getText().toString();
        String owner = this.etxtOwner.getText().toString();
        String ssn = this.etxtSSN.getText().toString();
        String phone = this.etxtPhone.getText().toString();
        String email = this.etxtEmail.getText().toString();
        String address = this.etxtAddress.getText().toString();

        if(storeName.isEmpty() ||  owner.isEmpty() || ssn.isEmpty() ||
                phone.isEmpty() || email.isEmpty() || address.isEmpty()) {
            return;
        }

        Store store = new Store(storeName, owner);

        Intent output = new Intent();
        output.putExtra(RESULT_KEY, store);
        setResult(RESULT_OK, output);
        finish();
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        super.onBackPressed();
    }
}