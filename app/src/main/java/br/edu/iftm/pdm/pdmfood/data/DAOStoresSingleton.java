package br.edu.iftm.pdm.pdmfood.data;

import java.util.ArrayList;
import br.edu.iftm.pdm.pdmfood.model.Store;

public class DAOStoresSingleton {
    private static DAOStoresSingleton INSTANCE;
    private ArrayList<Store> stores;

    private DAOStoresSingleton() {
        this.stores = new ArrayList<>();
        DummyData.createData(this.stores);
    }

    public static DAOStoresSingleton getINSTANCE() {
        if (INSTANCE == null) {
            INSTANCE = new DAOStoresSingleton();
        }
        return INSTANCE;
    }

    public ArrayList<Store> getStores() {
        return this.stores;
    }

    public void addStore(Store store) {
        this.stores.add(store);
    }
}
