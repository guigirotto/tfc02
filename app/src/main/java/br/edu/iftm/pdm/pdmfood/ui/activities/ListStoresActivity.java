package br.edu.iftm.pdm.pdmfood.ui.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import br.edu.iftm.pdm.pdmfood.R;
import br.edu.iftm.pdm.pdmfood.data.DAOStoresSingleton;
import br.edu.iftm.pdm.pdmfood.model.Store;
import br.edu.iftm.pdm.pdmfood.ui.lists.stores.StoresDataAdapter;

public class ListStoresActivity extends AppCompatActivity {

    private RecyclerView rvStores;
    private StoresDataAdapter storesDataAdapter;
    private LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_stores);

        this.rvStores = findViewById(R.id.rvStores);

        this.layoutManager = new LinearLayoutManager(this);
        this.storesDataAdapter = new StoresDataAdapter(DAOStoresSingleton.getINSTANCE().getStores());
        this.storesDataAdapter.setOnClickStoreListener(new StoresDataAdapter.OnClickStoreListener() {
            @Override
            public void onClickStore(Store store) {
                showStoreInfo(store);
            }
        });
        DividerItemDecoration divider = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);

        this.rvStores.setLayoutManager(this.layoutManager);
        this.rvStores.setHasFixedSize(true);
        this.rvStores.addItemDecoration(divider);
        this.rvStores.setAdapter(this.storesDataAdapter);
    }

    private void showStoreInfo(Store store) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        View view = getLayoutInflater().inflate(R.layout.show_store_info_dialog, null);
        dialogBuilder.setView(view);
        TextView txtStoreName = view.findViewById(R.id.nomepizza);
        txtStoreName.setText(store.getName());
        TextView txtPhone = view.findViewById(R.id.preco);
        txtPhone.setText(store.getPhoneNumber());
        dialogBuilder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // do nothing
            }
        });
        dialogBuilder.create().show();
    }
}